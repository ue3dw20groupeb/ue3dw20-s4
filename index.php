<?php

	include 'inc.twig.php'; //Il manque le "e" de include et cela crée une erreur de syntaxe ce qui empeche le code de s'exécuter

	$template_index = $twig->loadTemplate('index.tpl');//Il manquait le ";" en fin de déclaration et cela crée une erreur de syntaxe ce qui empeche le code de s'exécuter

	$n_jours_previsions = 3; 

	$ville = "Limoges";

	//~ Clé API
	//~ Si besoin, vous pouvez générer votre propre clé d'API gratuitement, en créant 
	//~ votre propre compte ici : https://home.openweathermap.org/users/sign_up
	$apikey = "10eb2d60d4f267c79acb4814e95bc7dc";

	$data_url = 'http://api.openweathermap.org/data/2.5/forecast/daily?APPID='.$apikey.'&q='.$ville.',fr&lang=fr&units=metric&cnt='.$n_jours_previsions;

	$data_contenu = file_get_contents($data_url);
		
	$_data_array = json_decode($data_contenu, true);

	$_ville = $_data_array['city'];
	$_journees_meteo = $_data_array['list']; // Il ya une erreur de syntaxe sur liste ce qui crée une erreur d'interprétation

	for ($i = 0; $i < count($_journees_meteo); $i++) {
		$_meteo = getMeteoImage($_journees_meteo[$i]['weather'][0]['icon']);
		
		$_journees_meteo[$i]['meteo'] = $_meteo;
	}

	echo $template_index->render(array(
		'_journees_meteo'	=> $_journees_meteo,
		'_ville'			=> $ville,// Il manque le ">" après le signe "=" et cela crée une erreur de syntaxe ce qui empeche le code de s'exécuter
		'n_jours_previsions'=> $n_jours_previsions,// Il manque une ","
		'ville'             => $_ville//Il manque l'affichage de ville et cela crée une erreur d'affichage
	));

	function getMeteoImage($code){
		if(strpos($code, 'n'))
			return 'entypo-moon';
		

		$_icones_meteo =array(
'01d' => 'entypo-light-up',
'02d' => 'entypo-light-up','03d' => 'entypo-cloud',//Il manque une "," à la fin de la ligne et cela crée une erreur de syntaxe ce qui empeche le code de s'exécuter
'04d' => 'entypo-cloud','09d' => 'entypo-water', 
'10d' => 'entypo-water',
'11d' => 'entypo-flash',
'13d' => 'entypo-star', 
'50d' => 'entypo-air');

		if(array_key_exists($code, $_icones_meteo)){ //Il manque l'ouverture de l'"{" pour ouvrir la fonction et cela crée une erreur de syntaxe ce qui empeche le code de s'exécuter
			return $_icones_meteo[$code];
		}else{
			return 'entypo-help';
		}
	}
// Il manque la fermeture du php
?>
